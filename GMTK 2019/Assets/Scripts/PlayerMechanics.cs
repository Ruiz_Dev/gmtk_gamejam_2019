﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class PlayerMechanics : MonoBehaviour {

    public float speed = 10.0f;
    public float stamina = 100f;
    [SerializeField]private bool usingStamina = false;
    private bool isPushing = false;

    public float health = 0.0f;

    private Rigidbody2D rb;
    private Vector2 movement;

    private Vector3 mousePos;
    private Vector3 targetRotatePos;

    public bool prova = true;
    private bool die = false;

    private SkeletonAnimator skeletonAnim;
    private Animator anim;
    private Vector3 altMousePos;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();

        skeletonAnim = GetComponentInChildren<SkeletonAnimator>();
        anim = GetComponentInChildren<Animator>();
    }
	
	void Update () {

        if(prova)
        {
            movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            anim.SetFloat("speedX", movement.x);
            anim.SetFloat("speedY", movement.y);

            //mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            //mousePos.Normalize();

            altMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (altMousePos.x < transform.position.x)
                skeletonAnim.skeleton.flipX = false;
            else if (altMousePos.x > transform.position.x)
                skeletonAnim.skeleton.flipX = true;

            //float rot = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
            //transform.rotation = Quaternion.Euler(0f, 0f, rot - 90);

            anim.SetBool("Boost", usingStamina);
            anim.SetFloat("Stamina", stamina);
        }


        if (Input.GetMouseButton(0) && stamina > 0)
        {
            usingStamina = true;
            stamina -= 1;
        }
        else if(stamina < 2)
        {
            usingStamina = false;
        }

        RecoveryStamina();

    }

    void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && stamina > 0)
        {
            SpringCharacter(movement);
        }
        else
        {
            MoveCharatcer(movement);
        }
    }

    void MoveCharatcer(Vector2 dir)
    {
        rb.MovePosition((Vector2)transform.position + (dir * speed * Time.deltaTime));
    }

    void SpringCharacter(Vector2 dir)
    {
        rb.MovePosition((Vector2)transform.position + (dir * speed * 2 * Time.deltaTime));
    }

    void RecoveryStamina()
    {
        if (!usingStamina && stamina < 100)
        {
            stamina += 0.3f;
            if (stamina > 100) stamina = 100;
        }
    }

    public void GetImpulseByCollision(Vector2 force)
    {
        health += 18;
        rb.AddForce(force, ForceMode2D.Impulse);
    }

    void MinimumHeal()
    {
        if(health >= 100)
        {
            die = true;
            //cridar el gameManager
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
       if(col.gameObject.tag == "Wall")
       {
            health += 5;
       }
        if (col.gameObject.tag == "Player")
        {
            if (!isPushing)
            {
                health += 2;
            }
            else if (Input.GetMouseButton(1) && stamina > 20)
            {
                col.gameObject.GetComponent<PlayerMechanics>().GetImpulseByCollision(new Vector2(-20, 5));
            }
        }

    }
}
